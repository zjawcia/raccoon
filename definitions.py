import os
from dotenv import load_dotenv

load_dotenv()
ACCESS_TOKEN = os.getenv("ACCESS_TOKEN")

HOST = "156.17.9.181"
PORT = 8888
IMAGES_PATH = 'images/'
SENDED_PATH = 'sended/'
SQLITE_URL = "sqlite:///data.sqlite"

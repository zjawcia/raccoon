from sqlmodel import SQLModel, Session, create_engine, Field, select

from definitions import SQLITE_URL


class Email(SQLModel, table=True):
    email: str = Field(unique=True, primary_key=True)


engine = create_engine(SQLITE_URL)


def create_db_and_tables():
    SQLModel.metadata.create_all(engine)


def get_session() -> Session:
    with Session(engine) as session:
        yield session


if __name__ == "__main__":
    create_db_and_tables()
    with Session(engine) as session:
        u = session.exec(select(Email)).all()
        if not u:
            u1 = Email(email="juliusz@tarnowski.io")
            session.add(u1)
            session.commit()
            u = session.exec(select(Email)).all()
        print(u)

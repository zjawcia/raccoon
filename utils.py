import os
import random
from definitions import IMAGES_PATH, SENDED_PATH


def list_photos() -> list[str]:
    iterator = os.scandir(IMAGES_PATH)
    photos = [entry.name for entry in iterator]
    return photos


def select_photo() -> str:
    photos = list_photos()
    if photos:
        return random.choice(photos)
    else:
        return None


def move_photo(filename):
    current_path = IMAGES_PATH + filename
    target_path = SENDED_PATH + filename
    os.replace(current_path, target_path)

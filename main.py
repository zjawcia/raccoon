def greeting(name="World"):
    print(f"Hello, {name}!")


if __name__ == "__main__":
    greeting()

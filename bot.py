import logging
import shutil 

from telegram import __version__ as TG_VER
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import (
    Application,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
)

from definitions import ACCESS_TOKEN, IMAGES_PATH, SENDED_PATH
from utils import select_photo, move_photo


# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

logger = logging.getLogger(__name__)

 
async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Starts the conversation and asks the user about their gender."""
    user = update.message.from_user
    logger.info("User %s started the conversation.", user.first_name)
    await update.message.reply_text(
        "Hemlo! Czy wysłać zdjecie szopa? /szop Jeśli nie, to elo. /papa" 
    )


async def send_photo(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    user = update.message.from_user
    logger.info("User %s asks for the photo.", user.first_name)
    photo = select_photo()
    if photo:
        await update.message.reply_photo(
            IMAGES_PATH + photo
        )
        move_photo(photo)
    else:
        await update.message.reply_text(
            "Zdjęcia się skończyły T - T" 
        )
        

async def cancel(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Cancels and ends the conversation."""
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    await update.message.reply_text(
        "Z Panem Bogiem.", reply_markup=ReplyKeyboardRemove()
    )


def main() -> None:
    """Run the bot."""
    # Create the Application and pass it your bot's token.
    application = Application.builder().token(ACCESS_TOKEN).build()

    application.add_handler(CommandHandler("start", start))
    application.add_handler(CommandHandler("szop", send_photo))
    application.add_handler(CommandHandler("papa", cancel))
    application.run_polling()


if __name__ == "__main__":
    main()

